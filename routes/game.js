const express = require('express')
const config = require('../config.js')

const router = express.Router()

/* GET users listing. */
router.get('/', function (req, res, next) {
  if (typeof req.query.username === 'undefined') {
    res.render('error')
  } else if (typeof req.query.gameMode !== 'undefined') {
    res.render(req.query.gameMode + 'Game', {
      // Type : New/Join Game
      type: req.query.connectType,
      // Gamemode: Normal/Free/Draw against Humanity
      mode: req.query.gameMode,
      // Name of the user
      username: req.query.username,
      // Adress of the host
      host: config.host
    })
  } else if (typeof req.query.connectType !== 'undefined') {
    const client = req.app.get('redis')
    if(req.query.gameID === 'null'){
      res.render('error');
      return;
    }
    client.existsAsync('room'+req.query.gameID).then(function(roomExists){
      if(!roomExists){
        res.render('error');
        return;
      }
      client.hgetAsync('room' + req.query.gameID, 'gameState').then(function(state){
        console.log(state);
        if(state !== 'WaitingForPlayers'){
          res.render('error')
          return;
        }
        client.hgetAsync('room' + req.query.gameID, 'gameMode').then(function (mode) {
          console.log(mode)
          client.llen('clients' + req.query.gameID, function (err, clientCount) {
            console.log('Client is ' + clientCount)
            if (clientCount < 4) {
              client.hvals('usernames' + req.query.gameID, function (err, playerList) {
                console.log(playerList)
                res.render(mode + 'Game', {
                  // Type : New/Join Game
                  type: req.query.connectType,
                  // Gamemode: Normal/Free/Draw against Humanity
                  mode: mode,
                  // ID of the game
                  gameID: req.query.gameID,
                  // Name of the user
                  username: req.query.username,
                  // playerList
                  playerList: playerList,
                  // Adress of the host
                  host: config.host
                })
              })
            } else {
              res.render('error')
            }
          })
        })
      })
  })// CHECK WHAT TYPE OF GAME IT IS
  }
})

module.exports = router

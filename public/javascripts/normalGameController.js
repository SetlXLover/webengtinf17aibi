let playerListArr = []
//Sets up the gameFlow and necessary variables and functions
function setupGameFlow (gameID, username, mode, playerList) {
  socket = io(host)
  if (gameID !== '') {
    let data = {
      gameID: gameID,
      username: username
    }
    //Let server handle socket join
    socket.emit('join', data)
    let room = gameID
    console.log(gameID)
  } else {
    let gameID = null
    let data = {
      mode: mode,
      username: username
    }
    socket.emit('create', data)
    // room = socket.id+'game';
  }
  //Getting data back when sucessfully joining a room
  socket.on('room',
    function (data) {
      let room = data
      console.log('Joined room with id: ' + room)
      document.getElementById('gameIDCopy').innerText = room
    })
  //SETTING UP VARIABLES OF DOM ELEMENTS
  let dummyDiv = document.getElementById('dummyDiv')
  let WordDiv = document.getElementById('WordDiv')
  let WaitingForPlayerDiv = document.getElementById('WaitingForPlayerDiv')
  let SelectWordPhaseDiv = document.getElementById('SelectWordPhaseDiv')
  let DrawingPhaseDiv = [document.getElementsByClassName("DrawingPhaseDiv")[0],
                         document.getElementsByClassName("DrawingPhaseDiv")[1],]
  let SelectWinnerPhaseDiv = document.getElementById('SelectWinnerPhaseDiv')
  let WinnerPhaseDiv = document.getElementById('WinnerPhaseDiv')

  // SETTING UP PLAYER LIST
  //Check if other players are present in room (When joining a room)
  if (playerList !== '') {
    if (playerList.split(',').length <= 1) {
      let listNode = document.createElement('LI')
      listNode.setAttribute('id', playerList)
      let textNode = document.createTextNode(playerList + ' ')
      let winCount = document.createElement('SPAN')
      winCount.innerText = '0'
      winCount.setAttribute('id', playerList + 'winCount')
      listNode.appendChild(textNode)
      listNode.appendChild(winCount)
      document.getElementById('playerList').appendChild(listNode)
      playerListArr = [playerList,username]
    } else {
      playerList.split(',').forEach(function (player) {
        playerListArr.push(player)
        let listNode = document.createElement('LI')
        listNode.setAttribute('id', player)
        let textNode = document.createTextNode(player + ' ')
        let winCount = document.createElement('SPAN')
        winCount.innerText = '0'
        winCount.setAttribute('id', player + 'winCount')
        listNode.appendChild(textNode)
        listNode.appendChild(winCount)
        document.getElementById('playerList').appendChild(listNode)
      })
      playerListArr.push(username);
      playerList = playerListArr
    }
  }
  //Append own Username to Playerlist
  let listNode = document.createElement('LI')
  listNode.setAttribute('id', username)
  let textNode = document.createTextNode(username + ' ')
  let winCount = document.createElement('SPAN')
  winCount.innerText = '0'
  winCount.setAttribute('id', username + 'winCount')
  listNode.appendChild(textNode)
  listNode.appendChild(winCount)
  document.getElementById('playerList').appendChild(listNode)

  DrawingPhaseDiv[0].setAttribute('style', 'display:none')
  DrawingPhaseDiv[1].setAttribute('style', 'display:none')
  //Get Notified on game error
  socket.on('game error', function (msg) {
    alert(msg)
  })
  //Get notified when a player joins the room, change PlayerList accordingly
  socket.on('playerJoined', function (joinedPlayer) {
    let listNode = document.createElement('LI')
    listNode.setAttribute('id', joinedPlayer)
    let textNode = document.createTextNode(joinedPlayer + ' ')
    let winCount = document.createElement('SPAN')
    winCount.innerText = '0'
    winCount.setAttribute('id', joinedPlayer + 'winCount')
    listNode.appendChild(textNode)
    listNode.appendChild(winCount)
    document.getElementById('playerList').appendChild(listNode)
  })
  //Get notified when a player leaves the room, change PlayerList accordingly
  socket.on('playerLeft', function (leftPlayer) {
    let listNode = document.getElementById(leftPlayer)
    listNode.parentNode.removeChild(listNode)
  })

  //Start the Drawing phase, hide and unhide accordingly, set Timer for DrawingPhase
  socket.on('startDrawing', function (data) {
    console.log(data)
    WordDiv.setAttribute('style','display:block')
    WordDiv.innerText = data
    SelectWordPhaseDiv.setAttribute('style', 'display:none;')
    DrawingPhaseDiv[0].setAttribute('style', 'display:flex')
    DrawingPhaseDiv[1].setAttribute('style', 'display:flex')
    startTimer(60, 'sendCanvas')
  })
  //Start the selectWordPhase, hide and unhide accordingly, set Timer for Selecting a word
  socket.on('startSelectWord', function (words) {
    let selected = false

    document.getElementById('clockContainer').setAttribute('style', 'display:block')
    document.getElementById('dummyDiv').setAttribute('style', 'display:block')
    SelectWordPhaseDiv.setAttribute('style', 'display:flex;')
    WaitingForPlayerDiv.setAttribute('style', 'display:none;')
    for (let i = 0; i <= 2; i++) {
      document.getElementById('word' + i).innerText = words[i]
    }
    startTimer(15, 'selectWord')
  })
  //Change Playerscore according to winner of the round
  socket.on('winnerRound', function (winner) {
    let playerWinCount = document.getElementById(winner + 'winCount')
    playerWinCount.innerText = parseInt(playerWinCount.innerText, 10)+1
    SelectWinnerPhaseDiv.setAttribute('style', 'display:none;')
    while (SelectWinnerPhaseDiv.firstChild) {
      SelectWinnerPhaseDiv.removeChild(SelectWinnerPhaseDiv.firstChild);
    }
    WordDiv.innerText = '';
  })
  //Handle received Canvas from other clients, append accordingly and set timer to select a winner
  socket.on('canvas', function (data) {
    clear()
    let imageFoo = document.createElement('img')
    imageFoo.src = data[0]
    let canvasNumber = data[1]
    let canvas = document.getElementById('SelectWinnerPhaseDiv').appendChild(imageFoo)
    canvas.setAttribute('id', canvasNumber)
    canvas.setAttribute('class', 'canvas')
    canvas.setAttribute('onclick', 'selectWinner(' + canvasNumber + ')')
    SelectWinnerPhaseDiv.setAttribute('style', 'display:flex;')
    DrawingPhaseDiv[0].setAttribute('style', 'display:none')
    DrawingPhaseDiv[1].setAttribute('style', 'display:none')
    startTimer(20, 'selectCanvas')
  })
  //Show the final winner
  socket.on('showWinner', function (winner) {
    clear()
    let playerWinCount = document.getElementById(winner + 'winCount')
    playerWinCount.innerText = parseInt(playerWinCount.innerText, 10)+1
    SelectWinnerPhaseDiv.setAttribute('style', 'display:none;')
    while (SelectWinnerPhaseDiv.firstChild) {
      SelectWinnerPhaseDiv.removeChild(SelectWinnerPhaseDiv.firstChild);
    }
    WinnerPhaseDiv.setAttribute('style', 'display:flex;')
    document.getElementById('clockContainer').setAttribute('style', 'display:hidden')
    document.getElementById('dummyDiv').setAttribute('style', 'display:none')
    console.log(winner)
  })
  //When Socket connects to server
  socket.on('connect',
    function () {
      console.log(socket)
      let room = socket.rooms
    }
  )
}
//Copy GameID to Clipboard
function copyToClipboard (element) {
  let $temp = $('<input>')
  $('body').append($temp)
  $temp.val($(element).text()).select()
  document.execCommand('copy')
  $temp.remove()
}
//Select the Winner of a round
function selectWinner (Winner) {
  console.log('test')
  console.log(timer)
  socket.emit('Winner', Winner)
}
//Initiate the Game and start the SelectWordPhase
function initiateGame () {
  let selected = false
  socket.emit('startSelectWord')
}
//Send Vote for SelectWordPhase
function sendWordVote () {
  let selectedWord = document.querySelector('input[name = "selectWord"]:checked').id
  socket.emit('selectWord', parseInt(selectedWord))
}
//ContinueGame after a round finished
function continueGame () {
  SelectWordPhaseDiv.setAttribute('style', 'display:flex;')
  WinnerPhaseDiv.setAttribute('style', 'display:none;')
  socket.emit('startSelectWord')
}

//Handle join functionality
module.exports = function (client, socket) {
  socket.on('join', function (data) {
    //Check if room exists
    client.existsAsync('room' + data.gameID).then(function (res) {
      if (!res) {
        console.log('room does not exist')
        console.log('Input message error and handling here')
        return
      }
      //Check if socket is already in room
      if (data.gameID in socket.rooms) {
       return
      }
      //Check length of room, max amount if clients is 4
      client.llenAsync('clients' + data.gameID).then(function (clientCount) {
        console.log('Client Count is: ' + clientCount)
        if (!(clientCount < 4)) {
          console.log('room ' + data.gameID + ' is full!')
          return
        }
        //Change room data according to the join
        client.multi()
          .lpush('clients' + data.gameID, socket.id)
          .sort('clients' + data.gameID, 'ALPHA', 'STORE', 'clients' + data.gameID)
          .lrange('clients' + data.gameID, 0, -1)
          .hmset('usernames' + data.gameID, socket.id, data.username)
          .exec()
        socket.currentRoomId = data.gameID
        socket.join(data.gameID)
        socket.broadcast.to(data.gameID).emit('playerJoined', data.username)
      })
    })
  })
  return socket.currentRoomId
}
let DrawingDiv = document.getElementsByClassName('DrawingPhaseDiv')[1]
function setup () {
  frameRate(120)

  // CANVAS
  canvasSize = (windowWidth*0.47+windowHeight*0.47)/2
  let canvas = createCanvas(canvasSize, canvasSize)
  canvas.parent(select('#canvasParent'))
  canvas.id('p5Canvas')
  let htmlCanvas = document.getElementById('p5Canvas')

  htmlCanvas.setAttribute('class', 'mainCanvas')
  htmlCanvas.setAttribute('style', 'justify-self:center;')
  // VARIABLES
  let strokecolor = { r: 0, g: 0, b: 0 }
  let strokewidth = 1

  // BUTTONS OPTIONS
  let btnClearCanvas = select('#buttonClearCanvas')
  btnClearCanvas.mouseClicked(function () {
    clear()
  })

  let sliderStrokeWeight = select('#strokeSizeSlider')
  sliderStrokeWeight.mouseReleased(function () {
    strokeWeight(sliderStrokeWeight.elt.value)
  })

  // COLORWHEEL
  let colorWheel = iro.ColorPicker('#colorWheelDemo', {
    width: 100,
    height: 300
  })
  colorWheel.on('color:change', function (color, changes) {
    strokecolor = color.rgb
    stroke(strokecolor.r, strokecolor.g, strokecolor.b)
  })
}

function draw () {
  frameRate(60)
  // CHECK IF MOUSE OVER CANVAS AND PRESSED AND IF DISPLAY IS !== none
  if (
    checkVisibility() &&
        mouseX <= 600 &&
        mouseY <= 600 &&
        mouseX > 0 &&
        mouseY > 0 &&
        mouseIsPressed) {
    line(mouseX, mouseY, pmouseX, pmouseY)
  }
}

function windowResized() {
  resizeCanvas((windowWidth*0.47+windowHeight*0.47)/2, (windowWidth*0.47+windowHeight*0.47)/2)
}

function checkVisibility () {
  return !(DrawingDiv.offsetParent === null)
}

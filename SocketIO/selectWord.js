getRandom = require('../helperFunctions/getRandom.js');
checkVote = require('../helperFunctions/checkVote.js');
arraysMatch = require('../helperFunctions/arraysMatch.js');
//select word functionality
module.exports = function (client, socket, io) {
  socket.on('selectWord', function (data) {
    // VALIDATING USER INPUT
    if (!Number.isInteger(data) || data < 0 || data > 2) {
      return // Invalid User Input
    }
    console.log('User selected Word ' + data)
    console.log('currentRoom ' + socket.currentRoomId)
    //Check if gameState is SelectWord
    client.hgetAsync('room' + socket.currentRoomId, 'gameState').then(function (state) {
      console.log(state)
      if (state !== 'SelectWord') {
        return
      }
      //Save Data
      client.multi().lpush('sentInfo' + socket.currentRoomId, socket.id)
        .sort('sentInfo' + socket.currentRoomId, 'ALPHA', 'STORE', 'sentInfo' + socket.currentRoomId)
        .lrange('sentInfo' + socket.currentRoomId, 0, -1)
        .zincrby('votesWords' + socket.currentRoomId, 1, data)
        .lrange('clients' + socket.currentRoomId, 0, -1)
        .exec(function (err, replies) {
          console.log('MULTI got ' + replies.length + ' replies')
          replies.forEach(function (reply, index) {
            console.log('Reply ' + index + ': ' + reply.toString())
          })
          if (!(arraysMatch(replies[2], replies[4]))) {
            return
          }
          console.log('voting is over')
          //Get Maximum of Votes
          client.zpopmaxAsync('votesWords' + socket.currentRoomId).then(function (max) {
            //Get index word with max Votes
            client.lindexAsync('words' + socket.currentRoomId, max[0]).then(function (word){
              console.log('Max is:' + word)
              client.hset('room'+socket.currentRoomId, 'gameState', 'Drawing')
              client.del('sentInfo' + socket.currentRoomId)
              client.del('votesWord' +socket.currentRoomId)
              io.in(socket.currentRoomId).emit('startDrawing', word)
              setTimeout(function () {
                checkVote(socket.currentRoomId, client, io ,'Canvas');
              }, 62000)
            })
          })
        })
    })
  })
}
arraysMatch = require('../helperFunctions/arraysMatch.js');
getRandom = require('../helperFunctions/getRandom.js');
checkVote = require('../helperFunctions/checkVote.js');
//Select Winner Functionality
module.exports = function (client, socket, io) {
  socket.on('Winner', function (winner) {
    // VALIDATE USER INPUT
    if (!Number.isInteger(winner) || winner < 0 || winner > 2) {
      return // Invalid User Input
    }
    //Check if GameState is SelectWinner
    client.hgetAsync('room' + socket.currentRoomId, 'gameState').then(function (state) {
      if (state !== 'SelectWinner') {
        return
      }
      console.log(socket.id)
      //Check if Socket has already committed Info
      client.lrangeAsync('sentInfoWinner'+socket.currentRoomId, 0, -1).then(function(sentInfo){
        if(sentInfo.includes(socket.id)){
          return
        }
        //Save Data
        client.multi().lpush('sentInfoWinner' + socket.currentRoomId, socket.id)
          .sort('sentInfoWinner' + socket.currentRoomId, 'ALPHA', 'STORE', 'sentInfoWinner' + socket.currentRoomId)
          .lrange('sentInfoWinner' + socket.currentRoomId, 0, -1)
          .zincrby('votesCanvas' + socket.currentRoomId, 1, winner)
          .lrange('clients' + socket.currentRoomId, 0, -1)
          .exec(function (err, replies) {
            console.log('MULTI got ' + replies.length + ' replies')
            replies.forEach(function (reply, index) {
              console.log('Reply ' + index + ': ' + reply.toString())
            })
            //Check if all clients who sent info === all clients available
            if (arraysMatch(replies[2], replies[4])) {
              console.log('voting is over')
              //Get Maximum Vote
              client.zpopmaxAsync('votesCanvas' + socket.currentRoomId).then(function (max) {
                console.log(max)
                console.log('Max is:' + max[0])
                //Get all Clients
                client.hvalsAsync('usernames' + socket.currentRoomId).then(function (users) {
                  //Get round
                  client.hgetAsync('room' + socket.currentRoomId, 'round').then(function (round) {
                    //If it's round 3, show final Winner
                    if (round >= 2) {
                      io.in(socket.currentRoomId).emit('showWinner', users[max[0]])
                      client.del('sentInfoWinner' + socket.currentRoomId, 'votesCanvas' + socket.currentRoomId)
                      client.del('votesCanvas' + socket.currentRoomId, 'votesCanvas' + socket.currentRoomId)
                      console.log('Last Round, show final winner!')
                      client.hsetAsync('room'+socket.currentRoomId, 'gameState', 'WaitingForPlayers')
                    } else {
                      client.hincrbyAsync('room' + socket.currentRoomId, 'round', 1).then(function () {
                        io.in(socket.currentRoomId).emit('winnerRound', users[max[0]])
                        client.del('sentInfoWinner' + socket.currentRoomId, 'votesCanvas' + socket.currentRoomId)
                        client.del('votesCanvas' + socket.currentRoomId, 'votesCanvas' + socket.currentRoomId)
                        let wordPool = getRandom(io.words, 3)
                        client.hsetAsync('room'+socket.currentRoomId, 'gameState', 'SelectWord')
                        io.in(socket.currentRoomId).emit('startSelectWord', wordPool)
                      })
                    }
                  })
                })
              })
            }
          })
      })
    })
  })
}
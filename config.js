let config = {};

config.redis = {};
config.game = {};

config.redis.host ='redis';
config.redis.port = 6379;
config.host = 'http://localhost:3000/';

module.exports = config;
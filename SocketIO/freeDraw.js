//functions for the freeDraw mode
module.exports = function (client, socket, io) {
  //Handle Mouse events
  socket.on('mouse',
    function (data) {
      console.log("Received: 'mouse' " + data.x + ' ' + data.y + ' to room: ' + data.room)
      //Send to the other sockets in the room
      socket.broadcast.to(socket.currentRoomId).emit('mouse', data)
    }
  )

// HANDLE CLEAR COMMAND
  socket.on('clear',
    function () {
    //check if socket is room owner
      client.hgetAsync('room' + socket.currentRoomId, 'owner').then(function (owner) {
        if (owner !== socket.id) {
          socket.emit('game error', 'Only the game owner can clear the canvas')
          return
        }
        console.log('Received clear')
        io.to(socket.currentRoomId).emit('clear')
      })
    }
  )
}
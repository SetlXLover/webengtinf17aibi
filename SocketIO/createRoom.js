module.exports = function (client, socket, io) {
  //Handle room creation
  socket.on('create', function (data) {
    console.log(socket.id)
    // VALIDATING USER INPUT
    if (data.mode !== 'normal' && data.mode !== 'free') {
      console.log('RIP')
      return // Invalid User Input
    }

    socket.currentRoomId = socket.id + 'game'
    //Check if room already exists
    client.existsAsync('room' + socket.currentRoomId).then(function (res) {
      console.log(res)
      if (res) {
        console.log('room already exists')
        console.log('Input message error and handling here')
        return
      }
      //Initialize room and client data
      client.multi()
        .hmset('room' + socket.currentRoomId, 'owner', socket.id, 'gameMode', data.mode, 'gameState', 'WaitingForPlayers', 'round', 0)
        .lpush('clients' + socket.currentRoomId, socket.id)
        .hmset('usernames' + socket.currentRoomId, socket.id, data.username)
        .exec()
      //Make the socket join the room
      socket.join(socket.currentRoomId, function () {
        //Tell the client that the socket has joined
        socket.emit('room', socket.currentRoomId)
      })
    })
  })
  return socket.currentRoomId
}
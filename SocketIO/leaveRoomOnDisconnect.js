//Leaving room on disconnect functionality
module.exports = function (client, socket, io) {
  socket.on('disconnect', function () {
    if (socket.currentRoomId === 'none') {
      return
    }
    //Check if room exists in redis
    client.existsAsync('room' + socket.currentRoomId).then(function (roomExists) {
      if (!roomExists) {
        return
      }
      //Check if socket is room owner -> delete room
      client.hgetAsync('room' + socket.currentRoomId, 'owner').then(function (owner) {
        if (owner === socket.id) {
          client.DEL('room' + socket.currentRoomId, 'clients' + socket.currentRoomId, 'words' + socket.currentRoomId)
          io.in(socket.currentRoomId).emit('game error', 'Gameowner disconnected :(')
          console.log('Owner of room ' + socket.currentRoomId + ' disconnected!')
          console.log('Clearing room' + socket.currentRoomId)
          return
        }
        //Remove Socket from room
        client.lremAsync('clients' + socket.currentRoomId, 1, socket.id).then(function () {
          //Check if 0 clients are connected
          client.llenAsync('clients' + socket.currentRoomId).then(function (clientCount) {
            if (clientCount <= 0) {
              client.DEL('room' + socket.currentRoomId, 'clients' + socket.currentRoomId, 'words' + socket.currentRoomId)
              console.log('Clearing room' + socket.currentRoomId)
              return
            }
            //Tell other Clients that someone disconnected
            client.hget('usernames' + socket.currentRoomId, socket.id, function (err, res) {
              socket.broadcast.to(socket.currentRoomId).emit('playerLeft', res)
              client.hdel('usernames' + socket.currentRoomId, socket.id)
            })
          })
        })
        console.log(socket.id + ' disconnected')
      })
    })
  })
  return socket.currentRoomId
}
let strokewidth = 1
let strokecolor = { r: 0, g: 0, b: 0 }
function setup () {
  frameRate(120)
  // CANVAS
  let canvasSize = (windowWidth*0.6+windowHeight*0.6)/2
  let canvas = createCanvas(canvasSize, canvasSize)
  canvas.parent(select('#canvasParent'))
  canvas.id('p5Canvas')
  let htmlCanvas = document.getElementById('p5Canvas')

  htmlCanvas.setAttribute('class', 'mainCanvas')
  // VARIABLES

  // BUTTONS OPTIONS
  let btnClearCanvas = select('#buttonClearCanvas')
  btnClearCanvas.mouseClicked(function () {
    socket.emit('clear', room)
  })

  // SLIDER STROKEWEIGHT
  let sliderStrokeWeight = select('#strokeSizeSlider')
  sliderStrokeWeight.mouseReleased(function () {
    strokewidth = sliderStrokeWeight.elt.value
    strokeWeight(sliderStrokeWeight.elt.value)
  })

  // COLORWHEEL
  let colorWheel = iro.ColorPicker('#colorWheelDemo', {
    width: 100,
    height: 300
  })
  colorWheel.on('color:change', function (color, changes) {
    strokecolor = color.rgb
    stroke(strokecolor.r, strokecolor.g, strokecolor.b)
  })
  // SOCKETS
  //Draw a line when mouse data is sent
  socket.on('mouse',
    // When we receive data
    function (data) {
      console.log('test')
      stroke(data.color.r, data.color.g, data.color.b)
      strokeWeight(data.strokewidth)
      if (typeof data.prevLoc !== 'undefined') {
        line(data.x, data.y, data.prevLoc[0], data.prevLoc[1])
      } else {
        line(data.x, data.y, data.x, data.y)
      }
      strokeWeight(strokewidth)
      stroke(strokecolor.r, strokecolor.g, strokecolor.b)
    }
  )
  //Clear the Canvas
  socket.on('clear',
    function () {
      clear()
    }
  )
}
//Draw Lines, according to set color, stroke weigth and mouse position
function draw () {
  frameRate(60)
  // CHECK IF MOUSE OVER CANVAS AND PRESSED
  if (
    mouseX <= 600 &&
        mouseY <= 600 &&
        mouseX > 0 &&
        mouseY > 0 &&
        mouseIsPressed) {
    strokeWeight(strokewidth)
    let data = {
      x: mouseX,
      y: mouseY,
      color: strokecolor,
      strokewidth: strokewidth,
      room: room
    }
    data.prevLoc = [pmouseX, pmouseY]
    // Send that object to the socket
    line(mouseX, mouseY, pmouseX, pmouseY)
    console.log(data.room)
    socket.emit('mouse', data)
  }
}
//When Windows is resized resize Canvas
//THIS WILL RESET THE CANVAS
function windowResized() {
  resizeCanvas((windowWidth * 0.6 + windowHeight * 0.6) / 2, (windowWidth * 0.6 + windowHeight * 0.6) / 2)
}
function sliderStrokeWeightChanged (weight) {
  let strokewidth = weight
}

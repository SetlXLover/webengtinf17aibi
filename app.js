// -----------REQUIRE--------------//

const createError = require('http-errors')
const express = require('express')
const socketIo = require('socket.io')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const bluebird = require('bluebird')
const indexRouter = require('./routes/index')
const gameRouter = require('./routes/game')
const config = require('./config.js');
const app = express()

// -----------REDIS--------------//

const redis = require('redis')
bluebird.promisifyAll(redis.RedisClient.prototype)
const client = redis.createClient( config.redis.port, config.redis.host)
client.on('error', function (err) {
  console.log('Error ' + err)
})
app.set('redis', client)

// -----------SOCKET.IO----------------//
let io = socketIo()
let socketFunctionalities = require("./SocketIO")
app.io = io

// -----------VIEW ENGINE--------------//

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

// -----------APP.USE------------------//

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use('/bootstrap', express.static(path.join(__dirname, '/node_modules/bootstrap/dist/')))
app.use('/popper.js', express.static(path.join(__dirname, '/node_modules/popper.js/dist/umd/')))
app.use('/jquery', express.static(path.join(__dirname, '/node_modules/jquery/dist/')))
app.use('/p5', express.static(path.join(__dirname, '/node_modules/p5/lib/')))
app.use('/iro', express.static(path.join(__dirname, '/node_modules/@jaames/iro/dist/')))
app.use('/io', express.static(path.join(__dirname, '/node_modules/socket.io-client/dist/')))
app.use('/', indexRouter)
app.use('/game', gameRouter)
app.use('*', function (req, res, next) {
  next();
});
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.render('error');
})

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

// -----------WORD POOL-------------//

io.words = ['America', 'Balloon', 'Biscuit', 'Blanket', 'Chicken', 'Chimney', 'Country', 'Cupcake', 'Curtain', 'Diamond',
  'Eyebrow', 'Fireman', 'Florida', 'Germany', 'Harpoon', 'Husband', 'Lobster', 'Milkman', 'Morning', 'Octagon', 'Octopus',
  'Popcorn', 'Printer', 'Sandbox', 'Skyline', 'Spinach', 'Trailer', 'Unibrow', 'Wrinkle']

// -------HANDLE SOCKET EVENTS---------//

io.sockets.on('connection',
  function (socket) {
    socket.currentRoomId = 'none'
    console.log('We have a new client: ' + socket.id)

    socketFunctionalities.createRoom(client, socket, io)

    socketFunctionalities.joinRoom(client, socket)

    socketFunctionalities.leaveRoomOnDisconnect(client, socket, io)

    socketFunctionalities.selectWord(client, socket, io)

    socketFunctionalities.receiveCanvas(client, socket, io)

    socketFunctionalities.selectWinnerCanvas(client, socket, io)

    socketFunctionalities.gameFlow(client, socket, io)

    socketFunctionalities.freeDraw(client, socket, io)
  }
)

function forceSendCanvas (socket) {
}
module.exports = app

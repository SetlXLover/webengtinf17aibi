//Function for Handling the clock timer
let timer;
function startTimer (time, socketString) {
  let endDate = new Date()
  endDate.setSeconds(endDate.getSeconds() + time)
  endDate = endDate.getTime()
  console.log(endDate)
  //If another timer was active stop it
  clearInterval(timer)
  //Start a timer that counts down until time is reached
  timer = setInterval(function () {
    let now = new Date().getTime()
    let t = endDate - now

    if (t >= 0) {
      let mins = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60))
      let secs = Math.floor((t % (1000 * 60)) / 1000)

      document.getElementById('clockContainer').innerHTML = mins * 60 + secs
    } else {
      if (socketString === 'selectWord') {
        sendWordVote()
      } else if (socketString === 'sendCanvas') {
        let canvas = document.getElementById('p5Canvas')
        let dataUrl = canvas.toDataURL()
        socket.emit('canvas', dataUrl)
      }
      clearInterval(timer)
    }
  }, 1000)
}

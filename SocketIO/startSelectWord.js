getRandom = require('../helperFunctions/getRandom.js');
checkVote = require('../helperFunctions/checkVote.js');
//Start SelectWordPhase (Game Start)
module.exports = function (client, socket, io) {
  socket.on('startSelectWord', function () {
    //Check if the Client initiating is the owner of the room
    client.hgetAsync('room' + socket.currentRoomId, 'owner').then(function (owner) {
      if(owner !== socket.id){
        socket.emit('game error', 'Only Owner is permitted to start game')
        console.log('Only Owner is permitted to start game')
        return
      }
      //Check if enough players are connected
      client.llenAsync('clients' + socket.currentRoomId).then(function (clientCount) {
        console.log(socket.currentRoomId)
        console.log(clientCount + ' Clients are connected')
        if (!(clientCount > 2)) {
          io.in(socket.currentRoomId).emit('game error', 'Unfortunately you need 2 friends :)')
          return
        }
      //Generate words for selection
        let wordPool = getRandom(io.words, 3)
        console.log(wordPool);
        //Save Data
        client.multi().del('words' + socket.currentRoomId)
          .rpush('words' + socket.currentRoomId, wordPool[0], wordPool[1], wordPool[2])
          .hset('room'+socket.currentRoomId, 'gameState', 'SelectWord')
          .exec()
        //send words to clients
        io.in(socket.currentRoomId).emit('startSelectWord', wordPool)
        setTimeout(function () {
          checkVote(socket.currentRoomId, client, io ,'Words');
        }, 17000)
      })
    })
  })
}
arraysMatch = require('../helperFunctions/arraysMatch.js');
//Receive Canvas Functionality
module.exports = function (client, socket, io) {
  socket.on('canvas', function (data) {
    //Check if GameState is Drawing
    client.hgetAsync('room' + socket.currentRoomId, 'gameState').then(function (state) {
      if (state !== 'Drawing') {
        return
      }
      //Save Canvas and save that socket has sent his info
      client.multi().lpush('sentInfoCanvas' + socket.currentRoomId, socket.id)
        .sort('sentInfoCanvas' + socket.currentRoomId, 'ALPHA', 'STORE', 'sentInfoCanvas' + socket.currentRoomId)
        .lrange('sentInfoCanvas' + socket.currentRoomId, 0, -1)
        .zadd('canvas' + socket.currentRoomId, 0, socket.id + data) // Put socketID and Canvas in one data field
        .lrange('clients' + socket.currentRoomId, 0, -1)
        .zrange('canvas' + socket.currentRoomId, 0, -1)
        .exec(function (err, replies) {
          console.log('MULTI got ' + replies.length + ' replies')
          replies.forEach(function (reply, index) {
            console.log('Reply ' + index + ': ' + reply.toString())
          })
          //Check if clients who sent info === all clients available
          if (arraysMatch(replies[2], replies[4])) {
            console.log('Drawing is over')
            client.hset('room'+socket.currentRoomId, 'gameState', 'SelectWinner')
            client.del('sentInfoCanvas' + socket.currentRoomId, 'canvas' + socket.currentRoomId)

            //Check if all clients that sent Info === all clients available and send pictures to clients
            //every client only receives pictures from clients different than himself
            replies[4].forEach(function (client) {
              let number = 0
              replies[4].forEach(function (canvas) {
                if (client !== canvas) {
                  console.log(replies[5][number])
                  io.to(client).emit('canvas', [replies[5][number].substring(20), number])
                }
                number++
              })
            })
            client.del('sentInfoCanvas' + socket.currentRoomId)
            client.del('canvas' + socket.currentRoomId)
            setTimeout(function () {
              checkVote(socket.currentRoomId, client, io ,'Winner');
            }, 22000)
          }
        })
    })
  })
}
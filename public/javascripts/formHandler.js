function prepare() {
  let typeSelector = document.getElementById("typeSelector");
  let modeSelector = document.getElementById("modeSelector");

  modeSelector.style.display = "none";
}
function chooseType(mode) {
  if(mode) {
    typeSelector.style.display = "none";
    modeSelector.style.display = "block";
  }
  if (!mode) {
    typeSelector.style.display = "block";
    modeSelector.style.display = "none";
  }
}
function cleanUpEmptyElements(element) {
  // Iterate through the available <input> elements
  let inputs = element.getElementsByTagName("input");
  for (let i = 0; i < inputs.length; i++) {
    // Disable those that do not have a value
    if (inputs[i].value.length === 0 && inputs[i].type !== 'submit') {
      inputs[i].disabled = 'disabled';
    }
  }
  // Continue your submission as normal
}

// Initiate SOCKET.IO
socket = io(host)
//Check if gameID is ''
if (gameID !== '') {
  let data = {
    gameID: gameID,
    username: username
  }
  //Join room and let other clients know
  socket.emit('join', data)
  room = gameID
  console.log(gameID)
} else {
  let gameID = null
  let data = {
    mode: mode,
    username: username
  }
  //Create room and let server know
  socket.emit('create', data)
  // room = socket.id+'game';
}
//Get notified when joining a room
socket.on('room',
  function (data) {
    room = data
    console.log('Joined room with id: ' + room)
    document.getElementById('gameIDCopy').innerText = room
  })
//Get notified when an error occurs
socket.on('game error', function (msg) {
  alert(msg)
})

//Copy GameID to Clipboard
function copyToClipboard (element) {
  let $temp = $('<input>')
  $('body').append($temp)
  $temp.val($(element).text()).select()
  document.execCommand('copy')
  $temp.remove()
}

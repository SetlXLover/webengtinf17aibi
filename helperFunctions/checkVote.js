getRandom = require('../helperFunctions/getRandom.js');
arraysMatch = require('../helperFunctions/arraysMatch.js');
//Function which is used to check wether or not a Vote has been successfully finished
//If not force the vote to finish
module.exports = function checkVote (room,client, io, reason) {
  if(reason ==='Words'){
    client.existsAsync('sentInfo'+room).then(function(result) {
      if (!result) {
        return
      }
      client.zpopmaxAsync('votes' + reason + room).then(function (max) {
        client.lindexAsync('words' + room, max[0]).then(function (word) {
          console.log('Max is:' + word)
          client.hset('room' + room, 'gameState', 'Drawing')
          client.del('sentInfo' + room)
          client.del('votes' + reason + room)
          io.in(room).emit('startDrawing', word)
          setTimeout(function () {
            checkVote(room, client, io, 'Canvas');
          }, 63000)
        })
      })
    })
  }else if(reason === 'Canvas'){
    client.existsAsync('sentInfoCanvas'+room).then(function(result) {
      if (!result) {
        return
      }
      client.multi().lrange('clients' + room, 0, -1)
        .zrange('canvas' + room, 0, -1)
        .exec(function (err, replies) {
          console.log('Drawing is over')
          client.hset('room' + room, 'gameState', 'SelectWinner')
          client.del('sentInfoCanvas' + room, 'canvas' + room)

          // Reihenfolge der Listen und Mengen sind gleich, daher:
          replies[0].forEach(function (client) {
            let number = 0
            replies[0].forEach(function (canvas) {
              if (client !== canvas) {
                console.log(replies[1][number])
                io.to(client).emit('canvas', [replies[1][number].substring(20), number])
              }
              number++
            })
          })
          client.del('sentInfoCanvas' + room)
          client.del('canvas' + room)
          setTimeout(function () {
            checkVote(room, client, io, 'Winner');
          }, 22000)
        })
    })
  } else if(reason === 'Winner'){
    client.existsAsync('sentInfoWinner'+room).then(function(result) {
      if (!result) {
        return
      }
      console.log('voting is over')
      client.zpopmaxAsync('votesCanvas' + room).then(function (max) {
        console.log(max)
        console.log('Max is:' + max[0])
        client.hvalsAsync('usernames' + room).then(function (users) {
          client.hgetAsync('room' + room, 'round').then(function (round) {
            if (round >= 5) {
              io.in(room).emit('showWinner', users[max[0]])
              client.del('sentInfoWinner' + room, 'votesCanvas' + room)
              client.del('votesCanvas' + room, 'votesCanvas' + room)
              console.log('Last Round, show final winner!')
              client.hsetAsync('room' + room, 'gameState', 'WaitingForPlayers')
            } else {
              client.hincrbyAsync('room' + room, 'round', 1).then(function () {
                io.in(room).emit('winnerRound', users[max[0]])
                client.del('sentInfo' + room, 'votesCanvas' + room)
                client.del('votesCanvas' + room, 'votesCanvas' + room)
                let wordPool = getRandom(io.words, 3)
                client.hsetAsync('room' + room, 'gameState', 'SelectWord')
                io.in(room).emit('startSelectWord', wordPool)
                setTimeout(function () {
                  checkVote(room, client, io, 'Words');
                }, 17000)
              })
            }
          })
        })
      })
    })
  }
}